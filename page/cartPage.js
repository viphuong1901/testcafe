import { Selector } from "testcafe";

class CartPage{
    constructor(){
        this.termLabel = Selector('input#termsofservice')
        this.carttotal = Selector('td.cart-total-right')
        this.checkoutBtn = Selector('button#checkout')
    }
}