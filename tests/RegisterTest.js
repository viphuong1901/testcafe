import { ClientFunction } from "testcafe";
import homePage from "../pages/homePage";
import registerPage from "../pages/registerPage";
import loginPage from "../pages/loginPage";
import CustomerPage from "../pages/CustomerPage";
import exp from "constants";
import { get, property } from "lodash";

const URL = 'https://demo.nopcommerce.com/';
const getURL = ClientFunction(() => window.location.href);
var randomNumber = Math.floor(Math.random() * 10000);
var useEmail = 'ponyyy'+randomNumber+'@gmail.com';

fixture('Registration Fixture')
    .page(URL)
    //hook
    .beforeEach(async t =>{
        await t
        .maximizeWindow()
        //.setTestSpeed(0.1)
        .setPageLoadTimeout(0)
    })

test('Assert Home Page test', async t =>{
    await t
        .expect(getURL()).eql(URL)
        .takeScreenshot()
        .expect(homePage.subtitleHeader.exists).ok();
});
test('User Registration and Login Test', async t =>{
    await t
        .click(homePage.registerLink)
        .expect(getURL()).contains('register')
        .click(registerPage.GenderOption)
        .typeText(registerPage.FirstName, 'Phuong')
        .typeText(registerPage.LastName, 'Vi');
        await registerPage.selectDay('19');
        await registerPage.selectMonth('January');
        await registerPage.selectYear('1999');
        await t
        .typeText(registerPage.Email, useEmail)
        .typeText(registerPage.Password, 'PhuongVi')
        .typeText(registerPage.ComfirmPassword, 'PhuongVi')
        .click(registerPage.RegisterButton)
        .expect(registerPage.SuccessfullMessage.exists).ok()

        //logout
        .click(homePage.logoutLink)

        //login with register account
        .click(homePage.loginlink)
        .expect(loginPage.accountHeader.exists).ok()
        .typeText(loginPage.emailInput,useEmail)
        .typeText(loginPage.passwordinput,'PhuongVi')
        .click(loginPage.submitButton)

        //go to acc
        .click(homePage.myAccountLink)

        //check order is displayed
        .expect(CustomerPage.orderLink.exists).ok()
        .click(CustomerPage.orderLink)
        .expect(CustomerPage.noOrdersLabel.exists).ok()
        //.takeScreenshot();
});