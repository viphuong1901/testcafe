import { Selector,t } from "testcafe";

class ProductDetailPage{
    constructor(){
        this.productPrice = Selector("span[id='price-value-4']").withText(' $1,800.00 ')
        this.productQuantity = Selector("div[class='min-qty-notification']")
        this.addToCart = Selector("input[id='product_enteredQuantity_4']")
        this.successMessage = Selector("button[id='add-to-cart-button-4']");
    }  
}
export default new ProductDetailPage();