import { ClientFunction } from "testcafe";
import homePage from "../pages/homePage";
import registerPage from "../pages/registerPage";
import SearchResultPage from "../pages/SearchResultPage";
import ProductDetailPage from "../pages/ProductDetailPage";
import cartpage from "../pages/CartPage";
import checkoutpage from "../pages/CheckoutPage";
//import MyOrderPage from "../pages/MyOrderPage";
import CustomerPage from "../pages/CustomerPage";

const URL = 'https://demo.nopcommerce.com/';
const getURL = ClientFunction(() => window.location.href);
var randomNumber = Math.floor(Math.random() * 10000);
var useEmail = 'ponyyy'+randomNumber+'@gmail.com';

fixture('E2E Fixture')
    .page(URL)
    //hook
    .beforeEach(async t =>{
        await t
        .maximizeWindow()
        //.setTestSpeed(0.1)
        .setPageLoadTimeout(0)
    })

test('Assert Home Page test', async t =>{
    await t
        .expect(getURL()).eql(URL)
        //.takeScreenshot()
        .expect(homePage.subtitleHeader.exists).ok();
});
test('Place Order E2E Tests', async t =>{
    await t
        .click(homePage.registerLink)
        .expect(getURL()).contains('register')
        .click(registerPage.GenderOption)
        .typeText(registerPage.FirstName, 'Phuong')
        .typeText(registerPage.LastName, 'Vi')
        .typeText(registerPage.Email, useEmail)
        .typeText(registerPage.Password, 'PhuongVi')
        .typeText(registerPage.ComfirmPassword, 'PhuongVi')
        .click(registerPage.RegisterButton)
        .expect(registerPage.SuccessfullMessage.exists).ok()
        await homePage.search('Apple MacBook Pro 13-inch')
        await t
            //search results
            .click(SearchResultPage.productTitle)
            .expect(getURL()).contains('apple-macbook-pro-13-inch')
            //product details
            .expect(ProductDetailPage.productPrice.exists).ok()
            .selectText(ProductDetailPage.productQuantity).pressKey("delete")
            .selectText(ProductDetailPage.productQuantity,'3')
            .typeText(ProductDetailPage.addToCart)
            .expect(ProductDetailPage.successMessage.exists).ok()
            .wait(3000)
            //cart and checkout
            .click(homePage.cartLink)
            .click(cartpage.termLabel)
            .click(cartpage.checkoutBtn)
            .expect(getURL()).contains('checkout');
            //place order
            await checkoutpage.countryList('Germany');
            await t
            .typeText(checkoutpage.cityTxt,'Berlin')
            .typeText(checkoutpage.address1Txt,'19 Pony Street')
            .typeText(checkoutpage.zipTxt,'12345')
            .typeText(checkoutpage.phoneTxt,'0334455')
            .click(checkoutpage.continueBtn)
            .click(checkoutpage.nextDayOption)
            .click(checkoutpage.nextShippingBtn)
            .click(checkoutpage.nextPaymentBtn)
            .click(checkoutpage.nextConfirmBtn)
            .click(checkoutpage.confirmOrderBtn)
            .expect(checkoutpage.orderConfirmationMessage.exists).ok()
            .click(checkoutpage.viewOrderDetailslink)
            //my account 
            .click(homePage.myAccountLink)
            .click(CustomerPage.orderLink);
});
test('Change Currency Test', async t =>{
    await homePage.changeCurrency('Euro');
});